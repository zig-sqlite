const std = @import("std");
const builtin = @import("builtin");

const c = @cImport({
    @cInclude("sqlite3.h");
});

const sqlite3 = @This();

const LOGGER = std.log.scoped(.sqlite);

/// The lifetime of a statement.
pub const StatementLifetime = enum {
    /// Expected to be disposed soon after creation.
    Ephemeral,
    /// Expected to be disposed long after creation.
    Persistent,
};

/// The type of SQLite row values.
pub const Type = enum {
    /// Integer number (`INTEGER`).
    Int,
    /// Floating point number (`REAL`).
    Float,
    /// Textual data (`TEXT`).
    Text,
    /// Binary data (`BLOB`).
    Blob,
    /// The absence of a value (`NULL`).
    Null,
};

pub const Config = struct {
    logger: ?*const fn (*anyopaque, c_int, [*c]const u8) callconv(.C) void = &adapter.log,
    allocator: ?std.mem.Allocator = null,
};

/// Initialize global configuration for all database connections.
///
/// Not thread safe.
pub fn initialize(config: Config) !void {
    if (config.logger) |logger|
        try ok(c.sqlite3_config(c.SQLITE_CONFIG_LOG, logger));
    if (config.allocator) |a| {
        adapter.allocator = a;
        try ok(c.sqlite3_config(c.SQLITE_CONFIG_MALLOC, &adapter.memory));
    }
    try ok(c.sqlite3_initialize());
}

pub fn shutdown() void {
    ok(c.sqlite3_shutdown()) catch
        unreachable;
}

const adapter = struct {
    fn log(ctx: *anyopaque, err: c_int, msg: [*c]const u8) callconv(.C) void {
        _ = ctx;
        LOGGER.err("({d}) {s}", .{ err, msg });
    }

    var allocator: std.mem.Allocator = undefined;

    const memory = c.sqlite3_mem_methods{
        .xMalloc = &malloc,
        .xFree = &free,
        .xRealloc = &realloc,
        .xSize = &allocationSize,
        .xRoundup = &roundup,
        .xInit = &init,
        .xShutdown = &deinit,
        .pAppData = null,
    };

    const natural_alignment = @alignOf(std.c.max_align_t);

    const AllocationNode = extern struct {
        size: usize,
        start: void align(natural_alignment),
    };

    inline fn nodeBytes(ptr: *align(natural_alignment) anyopaque) []align(@alignOf(AllocationNode)) u8 {
        const node: *AllocationNode = @fieldParentPtr("start", @as(*align(natural_alignment) void, @ptrCast(ptr)));
        return @as([*]align(@alignOf(AllocationNode)) u8, @ptrCast(node))[0..node.size];
    }

    pub fn malloc(size: c_int) callconv(.C) ?*anyopaque {
        const full_size = @offsetOf(AllocationNode, "start") + @as(usize, @intCast(size));
        if (allocator.alignedAlloc(u8, @alignOf(AllocationNode), full_size)) |slice| {
            const node: *AllocationNode = @ptrCast(slice.ptr);
            node.size = full_size;
            return &node.start;
        } else |_| {
            return null;
        }
    }

    pub fn free(maybe_ptr: ?*anyopaque) callconv(.C) void {
        const ptr = maybe_ptr orelse
            return;
        allocator.free(nodeBytes(@alignCast(ptr)));
    }

    pub fn realloc(maybe_ptr: ?*anyopaque, size: c_int) callconv(.C) ?*anyopaque {
        const ptr = maybe_ptr orelse
            return malloc(size);
        const full_size = @offsetOf(AllocationNode, "start") + @as(usize, @intCast(size));
        if (allocator.realloc(nodeBytes(@alignCast(ptr)), full_size)) |slice| {
            const node: *AllocationNode = @ptrCast(slice.ptr);
            node.size = slice.len;
            return &node.start;
        } else |_| {
            return null;
        }
    }

    pub fn allocationSize(maybe_ptr: ?*anyopaque) callconv(.C) c_int {
        const ptr = maybe_ptr orelse
            return 0;
        return @intCast(nodeBytes(@alignCast(ptr)).len - @offsetOf(AllocationNode, "start"));
    }

    pub fn roundup(size: c_int) callconv(.C) c_int {
        return std.mem.alignForward(c_int, @intCast(size), natural_alignment);
    }

    pub fn init(_: ?*anyopaque) callconv(.C) c_int {
        return c.SQLITE_OK;
    }

    pub fn deinit(_: ?*anyopaque) callconv(.C) void {}
};

pub const open = Connection.open;

pub const Connection = struct {
    /// The raw pointer to the database connection.
    sqlite: *c.sqlite3,

    /// Open the database.
    pub fn open(uri: [*c]const u8) !Connection {
        var db: ?*c.sqlite3 = undefined;
        const flags =
            c.SQLITE_OPEN_NOMUTEX |
            c.SQLITE_OPEN_READWRITE |
            c.SQLITE_OPEN_CREATE |
            c.SQLITE_OPEN_URI |
            c.SQLITE_OPEN_EXRESCODE;
        try ok(c.sqlite3_open_v2(uri, &db, flags, null));
        return .{ .sqlite = db.? };
    }

    /// Deinitialize the database, erring if it is improperly finalized.
    pub fn close(self: Connection) void {
        std.debug.assert(c.sqlite3_close(self.sqlite) == c.SQLITE_OK);
    }

    /// Perform the given SQL statements, discarding any rows.
    pub fn exec(self: Connection, sql: [*c]const u8) !void {
        try ok(c.sqlite3_exec(self.sqlite, sql, null, null, null));
    }

    test exec {
        try sqlite3.initialize(.{ .logger = null, .allocator = std.testing.allocator });
        defer sqlite3.shutdown();
        var db = try sqlite3.open("file::memory:?mode=memory");
        defer db.close();
        try db.exec("SELECT 1; SELECT 2;");
        try std.testing.expectError(error.SqliteError, db.exec("SELECT 1 SELECT 2;"));
    }

    /// Begin a transaction.
    pub inline fn begin(self: Connection) !void {
        try self.exec("BEGIN TRANSACTION;");
    }

    /// Commit a transaction.
    pub inline fn commit(self: Connection) !void {
        try self.exec("COMMIT TRANSACTION;");
    }

    /// Rollback a transaction logging any error.
    ///
    /// This function is infallible so that it may be used in an `errdefer`. It is a simple wrapper over
    /// `exec("ROLLBACK TRANSACTION;")`, so if fallible behavior is desired, then use that.
    pub inline fn rollback(self: Connection) void {
        self.exec("ROLLBACK TRANSACTION;") catch |err| LOGGER.err(
            "Error while rolling back transaction: {any}",
            .{@errorReturnTrace() orelse err},
        );
    }

    /// Return a prepared, singular SQL statement.
    ///
    /// Asserts `sql` is a valid UTF-8 encoded string.
    /// Asserts `sql` is fully processed by SQLite (i.e.: there is only one statement).
    pub fn prepare(
        /// The database connection.
        self: Connection,
        /// If `.Ephemeral`, sqlite will use optimizations which favor short lived statements (i.e.:
        /// allocating using lookaside memory). `.Persistent` has the inverse behavior, optimizing for
        /// long lived statements.
        lifetime: StatementLifetime,
        /// UTF-8 encoded string of SQL containing a single statement.
        sql: []const u8,
    ) !Statement {
        std.debug.assert(std.unicode.utf8ValidateSlice(sql));
        var stmt: ?*c.struct_sqlite3_stmt = undefined;
        var tail: [*c]const u8 = undefined;
        // TODO: Audit this for extended error codes or non-error primary result
        // codes.
        try ok(c.sqlite3_prepare_v3(
            self.sqlite,
            sql.ptr,
            @intCast(sql.len),
            switch (lifetime) {
                .Persistent => c.SQLITE_PREPARE_PERSISTENT,
                .Ephemeral => 0,
            },
            &stmt,
            &tail,
        ));
        std.debug.assert(tail == sql.ptr + sql.len);
        return .{ .stmt = stmt.? };
    }

    /// Prepare many SQL statements and return them as the collection `T`.
    pub fn prepareMany(
        /// The database connection.
        self: Connection,
        /// The collection into which the statements should be placed. The declaration order of the
        /// fields defines the order in which the statements should be defined in `sql`.
        comptime T: type,
        /// If `.Ephemeral`, SQLite will use optimizations which favor short lived statements (i.e.:
        /// allocating using lookaside memory). `.Persistent` has the inverse behavior, optimizing for
        /// long lived statements.
        lifetime: StatementLifetime,
        /// UTF-8 encoded string of SQL containing one or more statements.
        sql: []const u8,
    ) !T {
        std.debug.assert(std.unicode.utf8ValidateSlice(sql));
        const flags: c_uint = switch (lifetime) {
            .Persistent => c.SQLITE_PREPARE_PERSISTENT,
            .Ephemeral => 0,
        };
        var res: T = undefined;
        const end: [*c]const u8 = sql.ptr + sql.len;
        var head: [*c]const u8 = sql.ptr;
        var init_monotonic: usize = 0;
        errdefer inline for (comptime std.meta.fieldNames(T), 0..) |name, idx| {
            if (init_monotonic > idx)
                @field(res, name).finalize();
        };
        inline for (comptime std.meta.fieldNames(T), 1..) |name, nth| {
            var stmt: ?*c.struct_sqlite3_stmt = undefined;
            try ok(c.sqlite3_prepare_v3(
                self.sqlite,
                head,
                @intCast(@intFromPtr(end) - @intFromPtr(head)),
                flags,
                &stmt,
                &head,
            ));
            @field(res, name) = .{ .stmt = stmt.? };
            init_monotonic = nth;
        }
        while (head != end) : (head += 1) {
            std.debug.assert(std.ascii.isWhitespace(head.*));
        }
        return res;
    }

    /// Define a new SQL function for the given connection.
    pub fn createFunction(self: Connection, comptime func: Function, opts: CreateFunctionOptions) !void {
        var flags: c_int = switch (opts.encoding) {
            .Utf8 => c.SQLITE_UTF8,
        };
        if (opts.deterministic)
            flags |= c.SQLITE_DETERMINISTIC;
        if (opts.direct_only)
            flags |= c.SQLITE_DIRECTONLY;
        switch (func) {
            .scalar => |scalar| {
                try ok(c.sqlite3_create_function_v2(
                    self.sqlite,
                    opts.name,
                    opts.args,
                    flags,
                    opts.data,
                    wrapFunction(scalar),
                    null,
                    null,
                    opts.deinit,
                ));
            },
        }
    }

    test createFunction {
        const S = struct {
            pub fn hash(ctx: FunctionContext, args: []const Value) void {
                var hasher = std.crypto.hash.sha2.Sha256.init(.{});
                for (args) |arg| {
                    switch (arg.type()) {
                        .Blob => {},
                        else => ctx.err("expected all arguments to be blobs"),
                    }
                    const blob = arg.decode([]const u8) catch unreachable;
                    hasher.update(&std.mem.toBytes(std.mem.nativeToBig(u32, @intCast(blob.len))));
                    hasher.update(blob);
                }
                const digest = hasher.finalResult();
                ctx.result(digest, .{});
            }
        };
        try sqlite3.initialize(.{ .logger = null, .allocator = std.testing.allocator });
        defer sqlite3.shutdown();
        var db = try sqlite3.open("file::memory:?mode=memory");
        defer db.close();
        try db.createFunction(.{ .scalar = S.hash }, .{
            .name = "hash",
            .deterministic = true,
            .direct_only = false,
            .args = -1,
        });
        try db.exec("SELECT hash(CAST('abc' AS BLOB));");
    }
};

/// A SQLite prepared statement.
pub const Statement = struct {
    stmt: *c.struct_sqlite3_stmt,

    /// Return a reference to the database handle within which the statement was prepared.
    pub inline fn database(self: Statement) Connection {
        return .{ .sqlite = c.sqlite3_db_handle(self.stmt).? };
    }

    /// Advance to the next row produced by executing this statement.
    ///
    /// Returns:
    ///     `true` if a row was loaded.
    pub inline fn step(self: Statement) !bool {
        if (comptime std.log.logEnabled(.debug, .sqlite)) {
            const msg = c.sqlite3_expanded_sql(self.stmt);
            if (msg == null) {
                LOGGER.warn("Insufficient memory to expand SQL for debugging.", .{});
            } else {
                defer c.sqlite3_free(msg);
                LOGGER.debug("\n{s}", .{msg});
            }
        }
        return self._step();
    }

    fn _step(self: Statement) !bool {
        return switch (try result(c.sqlite3_step(self.stmt))) {
            .Ok => unreachable,
            .Row => true,
            .Done => false,
        };
    }

    /// Run the statement, asserting there are no results.
    ///
    /// Args:
    ///     self: The prepared statement which should be stepped forward.
    pub fn exec(self: Statement) !void {
        const more = try self.step();
        std.debug.assert(!more);
    }

    /// Run the statement and return a single column from a single row interpreted as `T`.
    ///
    /// Asserts there is a row.
    /// Asserts there is only one row.
    /// Asserts there is only one column.
    pub fn getScalar(
        /// The prepared statement which should be stepped forward.
        self: Statement,
        /// The type which should be read from the only column in the only row.
        comptime T: type,
    ) !T {
        const res = try self.stepScalar(T) orelse unreachable;
        return res;
    }

    /// Return the next `T` extracted from the only column if any row exists.
    ///
    /// Asserts there is only one column.
    pub fn stepScalar(
        /// The prepared statement which should be stepped forward.
        self: Statement,
        /// The type which should be read from the only column in the row.
        comptime T: type,
    ) !?T {
        if (!try self.step()) return null;
        // The assertion should get optimized out, but the C function call never will. Use comptime
        // logic to manually omit the call.
        if (comptime builtin.mode == .Debug or builtin.mode == .ReleaseSafe)
            std.debug.assert(c.sqlite3_column_count(self.stmt) == 1);
        return try self.column(T, 0);
    }

    /// Run the statement and return a single row interpreted as `T`.
    ///
    /// Asserts there is a row.
    /// Asserts there is only one row.
    pub fn getAs(
        /// The prepared statement which should be stepped forward.
        self: Statement,
        /// The type which should be read from the row.
        comptime T: type,
    ) !T {
        const res = try self.stepAs(T) orelse unreachable;
        return res;
    }

    /// Return the next `T` if any exists.
    ///
    /// The mapping between columns and fields is determined by declaration order of the fields.
    ///
    /// See Also: `stepAsOrdered`
    pub inline fn stepAs(
        /// The prepared statement which should be stepped forward.
        self: Statement,
        /// The type which should be read from the row.
        comptime T: type,
    ) !?T {
        return self.stepAsOrdered(T, comptime std.enums.values(std.meta.FieldEnum(T)));
    }

    /// Return the next `T` if any exists.
    ///
    /// Args:
    pub fn stepAsOrdered(
        /// The prepared statement which should be stepped forward.
        self: Statement,
        /// The type which should be read from the row.
        comptime T: type,
        /// An iterable of `std.meta.FieldEnum(T)` (or `@Type(.EnumLiteral)`) which represent the
        /// mapping between column's and fields.
        comptime order: anytype,
    ) !?T {
        if (!try self.step()) return null;
        var res: T = undefined;
        inline for (order, 0..) |fld, idx| {
            const val = try self.column(std.meta.fieldInfo(T, fld).type, idx);
            @field(res, @tagName(fld)) = val;
        }
        // TODO: Implement field defaulting for any fields which are
        // missing from `order`.
        return res;
    }

    /// Associate the given value with the parameter.
    ///
    /// `val` is bound differently based upon it's type. The following table
    /// indicates the mapping and their lifetimes:
    ///
    /// | Type or Kind     | SQL Type                               | Lifetime                                        |
    /// | ---------------- | -------------------------------------- | ----------------------------------------------- |
    /// | `.Int`           | `INTEGER`                              | Transient                                       |
    /// | `.ComptimeInt`   | `INTEGER`                              | Transient                                       |
    /// | `.Float`         | `REAL`                                 | Transient                                       |
    /// | `.ComptimeFloat` | `REAL`                                 | Transient                                       |
    /// | `[]const u8`     | `BLOB` or `TEXT`, default `BLOB`       | Static or Transient, default Static             |
    /// | `*[N]u8`         | `BLOB` or `TEXT`, default `BLOB`       | Static or Transient, default Static             |
    /// | `[N]u8`          | `BLOB` or `TEXT`, default `BLOB`       | Transient                                       |
    /// | `.Enum`          | `TEXT` or `INTEGER`, default `INTEGER` | Transient                                       |
    /// | `?U`             | `NULL` or recursively resolved         | `null` is transient, otherwise depends upon `U` |
    /// | `*U`             | Recursively resolves                   | Depends upon `U`                                |
    ///
    /// The SQL type is controlled by `opts.encoding` and the lifetime is controlled by
    /// `opts.lifetime`. Transient lifetime means SQLite creates a copy of the value in some
    /// fashion. For scalars and arrays, there is no other choice. For pointers, the lifetime may be
    /// static which means SQLite only takes a reference to the data. In that case, SQLite expects
    /// the data to "remain valid until either the prepared statement is finalized or the same SQL
    /// parameter is bound to something else, whichever occurs sooner"[^1].
    ///
    /// [^1]: https://www.sqlite.org/c3ref/bind_blob.html
    /// [^2]: https://www.sqlite.org/c3ref/bind_parameter_index.html
    pub fn bind(
        /// The statement to which the value should be bound.
        self: Statement,
        /// Either the parameter index or the parameter name. Using the parameter name incurs an
        /// additional cost. See sqlite3_bind_parameter_index[^2] for more information.
        param: anytype,
        /// The value which should be bound to the parameter.
        val: anytype,
        /// Options which decide how `val` should be used. See the table above for more information.
        comptime opts: EncodeOptions(@TypeOf(val)),
    ) !void {
        const param_idx: c_int = switch (@typeInfo(@TypeOf(param))) {
            .Int, .ComptimeInt => @intCast(param),
            else => blk: {
                std.debug.assert(std.unicode.utf8ValidateSlice(param));
                const idx = c.sqlite3_bind_parameter_index(self.stmt, param);
                if (idx == 0)
                    return error.UnknownParameter;
                break :blk idx;
            },
        };
        try ok(encode("sqlite3_bind", c_int, self.stmt, val, .{param_idx}, opts));
    }

    test bind {
        const E = enum(u8) { abc = 3, def = 1 };

        try sqlite3.initialize(.{ .logger = null, .allocator = std.testing.allocator });
        defer sqlite3.shutdown();
        var db = try sqlite3.open("file::memory:?mode=memory");
        defer db.close();
        var stmt = try db.prepare(.Ephemeral,
            \\SELECT
            \\  ?2,
            \\  ?1,
            \\  :int32,
            \\  :int64,
            \\  @fl,
            \\  $utf8,
            \\  :bl,
            \\  :option,
            \\  :enum_int,
            \\  :enum_utf8,
            \\  :ptr_int;
        );
        defer stmt.finalize();
        // Bind a comptime_int to a parameter index.
        try stmt.bind(1, 0xf00, .{});
        // Bind a comptime_float to a parameter index.
        try stmt.bind(2, 4.2, .{});
        // Bind a runtime integer.
        try stmt.bind(":int64", @as(i64, 0xdeadbeef), .{});
        // Bind a runtime integer.
        try stmt.bind(":int32", @as(i32, 0xffff), .{});
        // Bind a runtime float.
        try stmt.bind("@fl", @as(f64, 1.2), .{});
        // Bind a UTF-8 string.
        try stmt.bind("$utf8", "foo", .{ .encoding = .Utf8 });
        // Bind a blob.
        try stmt.bind(":bl", "\xde\xbe", .{ .encoding = .Blob });
        // Bind an optional type with a null value.
        try stmt.bind(":option", @as(?usize, null), .{});
        // Bind an enum encoded as an integer.
        try stmt.bind(":enum_int", E.abc, .{ .encoding = .Int });
        // Bind an enum encoded as the UTF-8 representation of it's tag name.
        try stmt.bind(":enum_utf8", E.def, .{ .encoding = .Utf8 });
        // Bind an pointer.
        try stmt.bind(":ptr_int", &@as(i32, 1), .{});
        try std.testing.expectEqualDeep(stmt.stepAs(struct {
            f64,
            usize,
            i32,
            i64,
            f64,
            []const u8,
            []const u8,
            ?usize,
            E,
            E,
            i32,
        }), .{
            4.2,
            0xf00,
            0xffff,
            0xdeadbeef,
            1.2,
            "foo",
            "\xde\xbe",
            null,
            .abc,
            .def,
            1,
        });
    }

    /// Query the statement for the type of the given `col`.
    pub fn columnType(self: Statement, col: c_int) Type {
        return switch (c.sqlite3_column_type(self.stmt, col)) {
            c.SQLITE_INTEGER => .Int,
            c.SQLITE_FLOAT => .Float,
            c.SQLITE_TEXT => .Text,
            c.SQLITE_BLOB => .Blob,
            c.SQLITE_NULL => .Null,
            else => unreachable,
        };
    }

    /// Marshal a column, `col`, into a value of type, `T`.
    ///
    /// | Type or Kind     | SQL Type                       | Ownership        |
    /// | ---------------- | ------------------------------ | ---------------- |
    /// | `.Int`           | `INTEGER`                      | Owned            |
    /// | `.ComptimeInt`   | `INTEGER`                      | Owned            |
    /// | `.Float`         | `REAL`                         | Owned            |
    /// | `.ComptimeFloat` | `REAL`                         | Owned            |
    /// | `[]const u8`     | `BLOB` or `TEXT`               | Borrowed         |
    /// | `.Enum`          | `TEXT` or `INTEGER`            | Owned            |
    /// | `?U`             | `NULL` or recursively resolved | Depends upon `U` |
    ///
    /// All other types are compile errors.
    ///
    /// Borrowed data becomes invalidated after any of the following events:
    ///
    /// - `step` or `reset` is called.
    /// - `finalize` is called; note that closing the database may implicitly
    ///   finalize the statement.
    /// - The same column is accessed using a different type which would cause
    ///   sqlite to perform implicit type conversion (i.e.: INT => TEXT or
    ///   similar).
    pub fn column(
        /// The statement from which to pull the column data.
        self: Statement,
        /// The type into which the column data should be marshaled.
        comptime T: type,
        /// The index of the column from which the data should be pulled.
        col: c_int,
    ) DecodeError(T)!T {
        return try decodeGeneric("sqlite3_column", self.stmt, T, .{col});
    }

    /// Convenience for resetting and clearing together.
    pub inline fn resetAndClear(self: Statement) void {
        self.reset();
        self.clear();
    }

    /// Clear the bindings applied to the statement.
    pub fn clear(self: Statement) void {
        _ = c.sqlite3_clear_bindings(self.stmt);
    }

    /// Reset the statement in preparation for stepping into the next row.
    pub fn reset(self: Statement) void {
        _ = c.sqlite3_reset(self.stmt);
    }

    /// Delete the statement and free it's resources.
    pub fn finalize(self: Statement) void {
        _ = c.sqlite3_finalize(self.stmt);
    }

    test column {
        const E = enum(u8) { abc = 3, def = 1 };

        try sqlite3.initialize(.{ .logger = null, .allocator = std.testing.allocator });
        defer sqlite3.shutdown();
        var db = try sqlite3.open("file::memory:?mode=memory");
        defer db.close();
        var stmt = try db.prepare(.Ephemeral,
            \\VALUES
            \\  (0, 'foo'),
            \\  (2, NULL),
            \\  (4.2, x'deadbeef'),
            \\  ('', x''),
            \\  ("abc", 1),
            \\  ("foo", 0);
        );
        defer stmt.finalize();
        // Verify tuples rows, 32-bit columns, and text columns.
        const S1 = struct { u32, []const u8 };
        try std.testing.expectEqualDeep(S1{ 0, "foo" }, stmt.stepAs(S1));
        // Verify struct rows, 64-bit columns, and optional columns.
        const S2 = struct { foo: u64, bar: ?[]const u8 };
        try std.testing.expectEqualDeep(S2{ .foo = 2, .bar = null }, stmt.stepAs(S2));
        // Verify re-ordered fields, blob columns, and float columns.
        const S3 = struct { foo: []const u8, bar: f64 };
        try std.testing.expectEqualDeep(
            S3{ .foo = "\xde\xad\xbe\xef", .bar = 4.2 },
            stmt.stepAsOrdered(S3, .{ .bar, .foo }),
        );
        // Verify empty text and blob columns.
        const S4 = struct { foo: []const u8, bar: []const u8 };
        try std.testing.expectEqualDeep(S4{ .foo = "", .bar = "" }, stmt.stepAs(S4));
        // Verify enum type in both text and integer column format.
        const S5 = struct { foo: E, bar: E };
        try std.testing.expectEqualDeep(S5{ .foo = .abc, .bar = .def }, stmt.stepAs(S5));
        // Verify invalid enum tags produce errors.
        const S6 = struct { foo: E, bar: E };
        try std.testing.expectError(error.InvalidEnumTag, stmt.stepAs(S6));
    }
};

/// A SQLite type-erased value.
pub const Value = packed struct {
    inner: *c.sqlite3_value,

    /// Decode `self` as a value of type `T`.
    pub fn decode(self: Value, comptime T: type) DecodeError(T)!T {
        return try decodeGeneric("sqlite3_value", self.inner, T, .{});
    }

    /// Return the `Type` of the underlying value.
    pub fn @"type"(self: Value) Type {
        return switch (c.sqlite3_value_type(self.inner)) {
            c.SQLITE_INTEGER => .Int,
            c.SQLITE_FLOAT => .Float,
            c.SQLITE_TEXT => .Text,
            c.SQLITE_BLOB => .Blob,
            c.SQLITE_NULL => .Null,
            else => unreachable,
        };
    }
};

/// The context passed to an application defined SQL function.
pub const FunctionContext = struct {
    inner: ?*c.sqlite3_context,

    /// Report a result value for the function call.
    ///
    /// This follows the same encoding rules as `Statement.bind`.
    pub fn result(
        self: FunctionContext,
        val: anytype,
        comptime opts: EncodeOptions(@TypeOf(val)),
    ) void {
        encode("sqlite3_result", void, self.inner, val, .{}, opts);
    }

    /// Report an error for the function call.
    pub fn err(self: FunctionContext, msg: []const u8) void {
        c.sqlite3_result_error(self.inner, msg.ptr, @intCast(msg.len));
    }

    /// Return the application defined data for this function if any.
    pub inline fn data(self: FunctionContext) ?*anyopaque {
        return c.sqlite3_user_data(self.inner);
    }
};

/// Options for tweaking how an application defined SQL function is used.
pub const CreateFunctionOptions = struct {
    /// The name of the function as used within SQL.
    name: [*:0]const u8,
    /// The number of arguments expected by the function.
    ///
    /// SQLite allows function overloading and uses this in determining the best fit.
    args: std.math.IntFittingRange(-1, 127),
    /// Whether or not there are side-effects of calling this function.
    deterministic: bool,
    /// Whether or not this may be run from schema-code.
    ///
    /// The SQLite documentation recommends setting this to `true` if it has "potentially dangerous
    /// side-effects, or if they could potentially leak sensitive information to an attacker if
    /// misused"[^1].
    ///
    /// [^1]: <https://sqlite.org/appfunc.html>
    direct_only: bool,
    /// The preferred text encoding when calling this function.
    ///
    /// Note that this module only defines UTF-8 interfaces but SQLite allows various other
    /// encodings.
    encoding: enum { Utf8 } = .Utf8,
    /// Application provided data which is necessary for executing the function.
    ///
    /// This is accessible via `FunctionContext.data`.
    data: ?*anyopaque = null,
    /// A deconstructor for `data`.
    deinit: ?*const fn (?*anyopaque) callconv(.C) void = null,
};

/// An application defined SQL function.
pub const Function = union(enum) {
    /// A SQL function which operates upon scalars and produces a scalar.
    scalar: fn (FunctionContext, []const Value) void,
};

fn wrapFunction(
    comptime func: fn (FunctionContext, []const Value) void,
) *const fn (?*c.sqlite3_context, c_int, [*c]?*c.sqlite3_value) callconv(.C) void {
    return &struct {
        fn wrapper(
            context: ?*c.sqlite3_context,
            n_args: c_int,
            args: [*c]?*c.sqlite3_value,
        ) callconv(.C) void {
            var args_slice: []const Value = undefined;
            args_slice.ptr = @ptrCast(args);
            args_slice.len = @intCast(n_args);
            func(.{ .inner = context }, args_slice);
        }
    }.wrapper;
}

/// Options for how to bind a given value with type, `T`.
///
/// See `bind` for more information.
pub fn EncodeOptions(comptime T: type) type {
    return switch (@typeInfo(T)) {
        .Pointer => |info| switch (info.size) {
            .Slice => EncodeBytesOptions,
            .One => switch (@typeInfo(info.child)) {
                .Array => |arr_info| if (arr_info.child == u8)
                    EncodeBytesOptions
                else
                    struct {},
                else => EncodeOptions(info.child),
            },
            .Many, .C => struct { lifetime: EncodeLifetime = .Static },
        },
        .Array => struct { encoding: EncodeBytesEncoding = .Blob },
        .Enum => struct {
            encoding: enum {
                Utf8,
                Int,
            } = .Int,
        },
        .Optional => |info| EncodeOptions(info.child),
        else => struct {},
    };
}

/// The lifetime of the data bound to a statement.
///
/// `.Static` means the data is given by reference to SQLite and is owned
/// by the application. `.Transient` means the data is copied into SQLite
/// and may be freely disposed of after `bind` returns. See `bind` for more
/// information.
const EncodeLifetime = enum { Static, Transient };

const EncodeBytesOptions = struct {
    encoding: EncodeBytesEncoding = .Blob,
    lifetime: EncodeLifetime = .Static,
};

const EncodeBytesEncoding = enum {
    Utf8,
    Blob,
};

fn encode(
    comptime prefix: []const u8,
    comptime Return: type,
    context: anytype,
    val: anytype,
    args: anytype,
    comptime opts: EncodeOptions(@TypeOf(val)),
) Return {
    const T = @TypeOf(val);
    switch (@typeInfo(T)) {
        .ComptimeInt => if (val > std.math.maxInt(c_int)) {
            return @call(.auto, @field(c, prefix ++ "_int64"), .{context} ++ args ++ .{val});
        } else {
            return @call(.auto, @field(c, prefix ++ "_int"), .{context} ++ args ++ .{val});
        },
        .Int => |info| if (info.bits > 32) {
            return @call(.auto, @field(c, prefix ++ "_int64"), .{context} ++ args ++ .{
                @as(c.sqlite3_int64, @intCast(val)),
            });
        } else {
            return @call(.auto, @field(c, prefix ++ "_int"), .{context} ++ args ++ .{
                @as(c_int, @intCast(val)),
            });
        },
        .ComptimeFloat, .Float => {
            return @call(.auto, @field(c, prefix ++ "_double"), .{context} ++ args ++ .{val});
        },
        .Pointer => |info| switch (info.size) {
            .Slice => if (info.child == u8) {
                const lifetime = switch (opts.lifetime) {
                    .Static => c.SQLITE_STATIC,
                    .Transient => c.SQLITE_TRANSIENT,
                };
                const CastT = if (@bitSizeOf(usize) > 32)
                    c.sqlite3_uint64
                else
                    c_int;
                switch (opts.encoding) {
                    .Utf8 => if (@bitSizeOf(usize) > 32) {
                        const func = @field(c, prefix ++ "_text64");
                        return @call(.auto, func, .{context} ++ args ++ .{
                            val.ptr,
                            @as(CastT, @intCast(val.len)),
                            lifetime,
                            c.SQLITE_UTF8,
                        });
                    } else {
                        const func = @field(c, prefix ++ "_text");
                        return @call(.auto, func, .{context} ++ args ++ .{
                            val.ptr,
                            @as(CastT, @intCast(val.len)),
                            lifetime,
                        });
                    },
                    .Blob => {
                        const func = if (@bitSizeOf(usize) > 32)
                            @field(c, prefix ++ "_blob64")
                        else
                            @field(c, prefix ++ "_blob");
                        return @call(.auto, func, .{context} ++ args ++ .{
                            val.ptr,
                            @as(CastT, @intCast(val.len)),
                            lifetime,
                        });
                    },
                }
            },
            .One => switch (@typeInfo(info.child)) {
                .Array => |arr_info| if (arr_info.child == u8) {
                    return encode(prefix, Return, context, @as([]const u8, val), args, opts);
                },
                else => return encode(prefix, Return, context, val.*, args, opts),
            },
            else => {},
        },
        .Array => |info| if (info.child == u8) {
            return encode(prefix, Return, context, @as([]const u8, &val), args, .{
                .encoding = opts.encoding,
                // Force SQLite to copy the bytes when binding.
                .lifetime = .Transient,
            });
        },
        .Optional => {
            if (val) |non_null| {
                return encode(prefix, Return, context, non_null, args, opts);
            }
            return @call(.auto, @field(c, prefix ++ "_null"), .{context} ++ args);
        },
        .Enum => switch (opts.encoding) {
            .Utf8 => {
                return encode(prefix, Return, context, @tagName(val), args, .{
                    .encoding = .Utf8,
                    .lifetime = .Static,
                });
            },
            .Int => {
                return encode(prefix, Return, context, @intFromEnum(val), args, .{});
            },
        },
        else => {},
    }
    @compileError("unsupported type " ++ @typeName(T));
}

/// Return the possible errors produced by marshaling `T` from a column.
pub fn DecodeError(comptime T: type) type {
    return switch (@typeInfo(T)) {
        .Pointer => error{OutOfMemory},
        .Optional => |info| DecodeError(info.child),
        .Enum => |info| error{InvalidEnumTag} ||
            DecodeError([]const u8) ||
            DecodeError(info.tag_type),
        else => error{},
    };
}

fn decodeGeneric(
    comptime prefix: []const u8,
    context: anytype,
    comptime T: type,
    args: anytype,
) DecodeError(T)!T {
    const value_type = @call(.auto, @field(c, prefix ++ "_type"), .{context} ++ args);
    switch (@typeInfo(T)) {
        .Int => |info| {
            switch (value_type) {
                c.SQLITE_INTEGER => {},
                else => unreachable,
            }
            if (info.bits > 32) {
                return @intCast(@call(.auto, @field(c, prefix ++ "_int64"), .{context} ++ args));
            } else {
                return @intCast(@call(.auto, @field(c, prefix ++ "_int"), .{context} ++ args));
            }
        },
        .Float => {
            switch (value_type) {
                c.SQLITE_FLOAT => {},
                else => unreachable,
            }
            return @floatCast(@call(.auto, @field(c, prefix ++ "_double"), .{context} ++ args));
        },
        .Pointer => |info| switch (info.size) {
            .Slice => if (info.is_const and info.child == u8) {
                // Note that sqlite will implicitly convert a `TEXT` type
                // column into a `BLOB` without any modification to the
                // data. This plays well with Zig's "strings" just being
                // bytes. If a user expects text, then they should assert
                // that after receiving this column.
                const ptr: [*c]const u8 = switch (value_type) {
                    c.SQLITE_BLOB => @ptrCast(@call(
                        .auto,
                        @field(c, prefix ++ "_blob"),
                        .{context} ++ args,
                    )),
                    c.SQLITE_TEXT => @call(
                        .auto,
                        @field(c, prefix ++ "_text"),
                        .{context} ++ args,
                    ),
                    else => unreachable,
                };
                const len = @call(.auto, @field(c, prefix ++ "_bytes"), .{context} ++ args);
                if (ptr == null) {
                    // If the length is zero, then it's just an empty blob.
                    if (len == 0)
                        return "";
                    return error.OutOfMemory;
                }
                var res: T = undefined;
                res.ptr = @ptrCast(ptr.?);
                res.len = @intCast(len);
                return res;
            },
            else => {},
        },
        .Optional => |info| {
            if (value_type == c.SQLITE_NULL)
                return null;
            return try decodeGeneric(prefix, context, info.child, args);
        },
        .Enum => |info| {
            switch (value_type) {
                c.SQLITE_TEXT => {
                    const str = try decodeGeneric(prefix, context, []const u8, args);
                    return std.meta.stringToEnum(T, str) orelse
                        error.InvalidEnumTag;
                },
                c.SQLITE_INTEGER => {
                    const int = try decodeGeneric(prefix, context, info.tag_type, args);
                    return try std.meta.intToEnum(T, int);
                },
                else => unreachable,
            }
        },
        else => {},
    }
    @compileError("unsupported type " ++ @typeName(T));
}

/// Process a sqlite result code into a Zig error.
///
/// If it is a non-error result code, it is debug asserted to be `.Ok`.
inline fn ok(res_int: c_int) !void {
    const res = try result(res_int);
    std.debug.assert(res == .Ok);
}

/// The possible successful SQLite status codes.
const Result = enum {
    /// The operation completed successfully.
    Ok,
    /// The query reached a new result row.
    Row,
    /// The query has completed.
    Done,
};

/// Process a sqlite result code into a Zig error or a `Result`.
///
/// This can handle either primary or extended result codes. Any unknown result
/// codes are converted to `error.SqliteUnexpected`.
fn result(res: c_int) !Result {
    return switch (res) {
        // The order of these are the same as the definition order in sqlite's
        // source.
        // Primary result codes.
        c.SQLITE_OK => .Ok,
        c.SQLITE_ROW => .Row,
        c.SQLITE_DONE => .Done,
        // Primary error codes.
        c.SQLITE_ERROR => error.SqliteError,
        c.SQLITE_INTERNAL => error.SqliteInternal,
        c.SQLITE_PERM => error.SqlitePerm,
        c.SQLITE_ABORT => error.SqliteAbort,
        c.SQLITE_BUSY => error.SqliteBusy,
        c.SQLITE_LOCKED => error.SqliteLocked,
        c.SQLITE_NOMEM => error.OutOfMemory,
        c.SQLITE_READONLY => error.SqliteReadonly,
        c.SQLITE_INTERRUPT => error.SqliteInterrupt,
        c.SQLITE_IOERR => error.InputError,
        c.SQLITE_CORRUPT => error.SqliteCorrupt,
        c.SQLITE_NOTFOUND => error.FileNotFound,
        c.SQLITE_FULL => error.SqliteFull,
        c.SQLITE_CANTOPEN => error.SqliteCantOpen,
        c.SQLITE_PROTOCOL => error.SqliteProtocol,
        c.SQLITE_EMPTY => error.SqliteEmpty,
        c.SQLITE_SCHEMA => error.SqliteSchema,
        c.SQLITE_TOOBIG => error.SqliteTooBig,
        c.SQLITE_CONSTRAINT => error.SqliteConstraint,
        c.SQLITE_MISMATCH => error.SqliteMismatch,
        c.SQLITE_MISUSE => error.SqliteMisuse,
        c.SQLITE_NOLFS => error.SqliteNoLfs,
        c.SQLITE_AUTH => error.AccessDenied,
        c.SQLITE_FORMAT => error.SqliteFormat,
        c.SQLITE_RANGE => error.SqliteRange,
        c.SQLITE_NOTADB => error.SqliteNotADb,
        // Extended error codes.
        c.SQLITE_ERROR_MISSING_COLLSEQ,
        c.SQLITE_ERROR_RETRY,
        c.SQLITE_ERROR_SNAPSHOT,
        => error.SqliteError,
        c.SQLITE_IOERR_READ,
        c.SQLITE_IOERR_SHORT_READ,
        c.SQLITE_IOERR_WRITE,
        c.SQLITE_IOERR_FSYNC,
        c.SQLITE_IOERR_DIR_FSYNC,
        c.SQLITE_IOERR_TRUNCATE,
        c.SQLITE_IOERR_FSTAT,
        c.SQLITE_IOERR_UNLOCK,
        c.SQLITE_IOERR_RDLOCK,
        c.SQLITE_IOERR_DELETE,
        c.SQLITE_IOERR_BLOCKED,
        => error.InputOutput,
        c.SQLITE_IOERR_NOMEM => error.OutOfMemory,
        c.SQLITE_IOERR_ACCESS => error.AccessDenied,
        c.SQLITE_IOERR_CHECKRESERVEDLOCK,
        c.SQLITE_IOERR_LOCK,
        c.SQLITE_IOERR_CLOSE,
        c.SQLITE_IOERR_DIR_CLOSE,
        c.SQLITE_IOERR_SHMOPEN,
        c.SQLITE_IOERR_SHMSIZE,
        c.SQLITE_IOERR_SHMLOCK,
        c.SQLITE_IOERR_SHMMAP,
        c.SQLITE_IOERR_SEEK,
        => error.InputOutput,
        c.SQLITE_IOERR_DELETE_NOENT => error.FileNotFound,
        c.SQLITE_IOERR_MMAP,
        c.SQLITE_IOERR_GETTEMPPATH,
        c.SQLITE_IOERR_CONVPATH,
        c.SQLITE_IOERR_VNODE,
        c.SQLITE_IOERR_AUTH,
        c.SQLITE_IOERR_BEGIN_ATOMIC,
        c.SQLITE_IOERR_COMMIT_ATOMIC,
        c.SQLITE_IOERR_ROLLBACK_ATOMIC,
        c.SQLITE_IOERR_DATA,
        c.SQLITE_IOERR_CORRUPTFS,
        => error.InputOutput,
        c.SQLITE_LOCKED_SHAREDCACHE,
        c.SQLITE_LOCKED_VTAB,
        => error.SqliteLocked,
        c.SQLITE_BUSY_RECOVERY,
        c.SQLITE_BUSY_SNAPSHOT,
        c.SQLITE_BUSY_TIMEOUT,
        => error.SqliteBusy,
        c.SQLITE_CANTOPEN_NOTEMPDIR,
        c.SQLITE_CANTOPEN_ISDIR,
        c.SQLITE_CANTOPEN_FULLPATH,
        c.SQLITE_CANTOPEN_CONVPATH,
        c.SQLITE_CANTOPEN_SYMLINK,
        => error.SqliteCantOpen,
        c.SQLITE_CORRUPT_VTAB,
        c.SQLITE_CORRUPT_SEQUENCE,
        c.SQLITE_CORRUPT_INDEX,
        => error.SqliteCorrupt,
        c.SQLITE_READONLY_RECOVERY,
        c.SQLITE_READONLY_CANTLOCK,
        c.SQLITE_READONLY_ROLLBACK,
        c.SQLITE_READONLY_DBMOVED,
        c.SQLITE_READONLY_CANTINIT,
        c.SQLITE_READONLY_DIRECTORY,
        => error.SqliteReadonly,
        c.SQLITE_ABORT_ROLLBACK => error.SqliteAbort,
        c.SQLITE_CONSTRAINT_CHECK,
        c.SQLITE_CONSTRAINT_COMMITHOOK,
        c.SQLITE_CONSTRAINT_FOREIGNKEY,
        c.SQLITE_CONSTRAINT_FUNCTION,
        c.SQLITE_CONSTRAINT_NOTNULL,
        c.SQLITE_CONSTRAINT_PRIMARYKEY,
        c.SQLITE_CONSTRAINT_TRIGGER,
        c.SQLITE_CONSTRAINT_UNIQUE,
        c.SQLITE_CONSTRAINT_VTAB,
        c.SQLITE_CONSTRAINT_ROWID,
        c.SQLITE_CONSTRAINT_PINNED,
        c.SQLITE_CONSTRAINT_DATATYPE,
        => error.SqliteConstraint,
        c.SQLITE_AUTH_USER => error.AccessDenied,
        else => error.SqliteUnexpected,
    };
}

test {
    std.testing.refAllDeclsRecursive(@This());
}
