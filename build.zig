const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const single_threaded = b.option(bool, "single-threaded", "Enable single-threaded mode.");
    const shared_cache = b.option(bool, "shared-cache", "Enable shared cache mode.") orelse
        false;
    const json1 = b.option(bool, "json1", "Enable JSON1 extension.") orelse
        true;
    const fts5 = b.option(bool, "fts5", "Enable the fts5 extension.") orelse
        false;
    const armor = b.option(bool, "armor", "Enable API Armor option.") orelse
        (optimize == .Debug or optimize == .ReleaseSafe);
    const debug = b.option(bool, "debug", "Enable SQLite's internal debug assertions.") orelse
        false;
    const anti_patterns = b.option(bool, "anti-patterns", "Whether to include deprecated APIs.") orelse
        false;

    const sqlite = b.addStaticLibrary(.{
        .name = "sqlite",
        .target = target,
        .optimize = optimize,
        .single_threaded = single_threaded,
    });
    {
        var sqlite_flags = std.ArrayList([]const u8).init(b.allocator);
        defer sqlite_flags.deinit();
        try sqlite_flags.appendSlice(&[_][]const u8{
            "-DSQLITE_ENABLE_NORMALIZE",
            "-DSQLITE_OMIT_AUTOINIT",
        });
        if (!shared_cache)
            try sqlite_flags.append("-DSQLITE_OMIT_SHARED_CACHE");
        if (json1)
            try sqlite_flags.append("-DSQLITE_ENABLE_JSON1");
        if (fts5)
            try sqlite_flags.append("-DSQLITE_ENABLE_FTS5");
        if (armor)
            try sqlite_flags.append("-DSQLITE_ENABLE_API_ARMOR");
        if (debug)
            try sqlite_flags.append("-DSQLITE_DEBUG");
        if (single_threaded == true)
            try sqlite_flags.append("-DSQLITE_THREADSAFE=0");
        if (anti_patterns) {
            try sqlite_flags.append("-DSQLITE_DQS");
        } else {
            try sqlite_flags.append("-DSQLITE_OMIT_DPRECATED");
        }
        sqlite.addCSourceFile(.{
            .file = b.path("third_party/sqlite3.c"),
            .flags = sqlite_flags.items,
        });
    }
    sqlite.installHeader(b.path("third_party/sqlite3.h"), "sqlite3.h");
    sqlite.installHeader(b.path("third_party/sqlite3ext.h"), "sqlite3ext.h");
    sqlite.linkLibC();

    const module = b.addModule("sqlite3", .{
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
        .single_threaded = single_threaded,
    });
    module.linkLibrary(sqlite);

    const unit_tests = b.addTest(.{
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
        .single_threaded = single_threaded,
    });
    unit_tests.linkLibrary(sqlite);

    const run_lib_unit_tests = b.addRunArtifact(unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_lib_unit_tests.step);

    // Generate API docs.
    const install_doc = b.addInstallDirectory(.{
        .source_dir = unit_tests.getEmittedDocs(),
        .install_dir = .prefix,
        .install_subdir = "doc",
    });
    const doc_step = b.step("doc", "Generate API docs");
    doc_step.dependOn(&install_doc.step);

    const coverage = b.addSystemCommand(&.{
        "kcov",
        b.fmt("--include-pattern={s}", .{b.pathFromRoot("src")}),
        b.pathFromRoot("kcov-out"),
    });
    coverage.addFileArg(unit_tests.getEmittedBin());
    const coverage_run = b.step("coverage", "Run unit tests while capturing coverage.");
    coverage_run.dependOn(&coverage.step);
}
